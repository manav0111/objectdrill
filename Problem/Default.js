const Default = (obj, defaultProp) => {
  //this fn will take the default Prop's if the default property is not there in our actual object

  for (let key in defaultProp) {
    //hasOwnProperty will check if that key exist or not
    if (defaultProp.hasOwnProperty(key) && obj[key] === undefined) {
      obj[key] = defaultProp[key];
    }
  }

  return obj;
};

module.exports = Default;
