const Pair = (obj) => {
  const result = [];

  //This fuction is Used to Create pair of key and value in the form of [key,value]
  for (let key in obj) {
    result.push([key, obj[key]]);
  }

  return result;
};

module.exports = Pair;
