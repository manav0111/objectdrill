const MapObject = (obj, callback) => {
  //This is used to map a key and value
  for (let key in obj) {
    callback(key, obj[key]);
  }
};

module.exports = MapObject;
