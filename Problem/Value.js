const Values = (obj) => {
  const result = [];

   //This function will retun the Values of an object
  for (let key in obj) {
    result.push(obj[key]);
  }

  return result;
};

module.exports = Values;
